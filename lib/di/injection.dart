import 'package:get/get.dart';
import 'package:mobius_test/data/data_source/local/todo_local_datasource.dart';
import 'package:mobius_test/data/repositories/todo_repo_impl.dart';
import 'package:mobius_test/domain/repositories/todo_repo.dart';
import 'package:mobius_test/domain/use_case/todo_use_case.dart';

class Injector {
  static final getxFind = Get.find;

  static Future<void> setupData() async {
    // di for local datasource
    Get.put<TodoLocalDataSource>(TodoLocalDataSourceImpl(), permanent: true);
  }

  static Future<void> setupDomain() async {
    // di for repositories
    Get.put<TodoRepo>(TodoRepoImpl(getxFind()), permanent: true);

    // di for use case
    Get.put<TodoUseCase>(TodoUseCase(getxFind()), permanent: true);
  }
}
