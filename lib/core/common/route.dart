import 'package:get/get.dart';
import 'package:mobius_test/presentation/addition_task/binding/addition_task_binding.dart';
import 'package:mobius_test/presentation/addition_task/view/addition_task_screen.dart';
import 'package:mobius_test/presentation/home/binding/home_binding.dart';
import 'package:mobius_test/presentation/home/view/home_screen.dart';
import 'package:mobius_test/presentation/splash/binding/splash_binding.dart';
import 'package:mobius_test/presentation/splash/view/splash_screen.dart';

class AppPage {
  static const String SPLASH = '/splash';
  static const String HOME = '/home';
  static const String ADDITION_TASK = '/addition_task';

  static final List<GetPage> pages = [
    GetPage(name: SPLASH, page: () => SplashScreen(), binding: SplashBinding()),
    GetPage(name: HOME, page: () => HomeScreen(), binding: HomeBinding()),
    GetPage(name: ADDITION_TASK, page: () => AdditionTaskScreen(), binding: AdditionTaskBinding()),
  ];
}
