import 'package:flutter/material.dart';

class AppTheme {
  final ThemeData lightTheme = ThemeData.light().copyWith(
    appBarTheme: const AppBarTheme(
      backgroundColor: Colors.white,
      elevation: 0.0,
      centerTitle: true,
      iconTheme: IconThemeData(
        color: Colors.black,
      ),
      titleTextStyle:
          TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold, color: Color(0xFF4F4F4F)),
    ),
    scaffoldBackgroundColor: Colors.white,
    primaryColor: const Color(0xFF315E38),
    textSelectionTheme: const TextSelectionThemeData(
        cursorColor: Color(0xFF315E38),
        selectionColor: Color(0xFF67CE80),
        selectionHandleColor: Color(0xFF96CCA1)),
    chipTheme: const ChipThemeData(
      selectedColor: Color(0xFF67CE80),
      backgroundColor: Colors.white,
    ),
    dividerTheme: const DividerThemeData(color: Color(0xFFCFCFCF)),
  );

  final ThemeData darkTheme = ThemeData.dark().copyWith(
    appBarTheme: const AppBarTheme(
      backgroundColor: Colors.white,
      elevation: 0.0,
      centerTitle: true,
      iconTheme: IconThemeData(
        color: Colors.black,
      ),
      titleTextStyle:
      TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold, color: Color(0xFF4F4F4F)),
    ),
    scaffoldBackgroundColor: Colors.white,
    primaryColor: const Color(0xFF315E38),
    textSelectionTheme: const TextSelectionThemeData(
        cursorColor: Color(0xFF315E38),
        selectionColor: Color(0xFF67CE80),
        selectionHandleColor: Color(0xFF96CCA1)),
    chipTheme: const ChipThemeData(
      selectedColor: Color(0xFF67CE80),
      backgroundColor: Colors.white,
    ),
    dividerTheme: const DividerThemeData(color: Color(0xFFCFCFCF)),
  );
}
