import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppColor {
  Color? primaryColor;
  Color? primaryMediumColor;
  Color? primaryPlateColor;
  Color? underlineColor;
  Color? gray1Color;
  Color? gray2Color;
  Color? gray3Color;
  Color? gray4Color;
  Color? gray5Color;
  Color? errorColor;
  Color? hintTextColor;
  Color? searchBoxFillColor;

  AppColor() {
    if (Get.isDarkMode) {
      setupDarkMode();
    } else {
      setupLightMode();
    }
  }

  void setupLightMode() {
    primaryColor = const Color(0xFF315E38);
    primaryMediumColor = const Color(0xFF67CE80);
    primaryPlateColor = const Color(0xFF96CCA1);
    underlineColor = const Color(0xFFE3E3E3);
    gray1Color = const Color(0xFF333333);
    gray2Color = const Color(0xFF4F4F4F);
    gray3Color = const Color(0xFF828282);
    gray4Color = const Color(0xFFBDBDBD);
    gray5Color = const Color(0xFFE0E0E0);
    errorColor = const Color(0xFFF44336);
    hintTextColor = const Color(0xFF8A8A8F);
    searchBoxFillColor = const Color(0xFFF2F2F2);
  }

  void setupDarkMode() {
    primaryColor = const Color(0xFF315E38);
    primaryMediumColor = const Color(0xFF67CE80);
    primaryPlateColor = const Color(0xFF96CCA1);
    underlineColor = const Color(0xFFE3E3E3);
    gray1Color = const Color(0xFF333333);
    gray2Color = const Color(0xFF4F4F4F);
    gray3Color = const Color(0xFF828282);
    gray4Color = const Color(0xFFBDBDBD);
    gray5Color = const Color(0xFFE0E0E0);
    errorColor = const Color(0xFFF44336);
    hintTextColor = const Color(0xFF8A8A8F);
    searchBoxFillColor = const Color(0xFFF2F2F2);
  }
}
