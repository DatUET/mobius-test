import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:mobius_test/core/app/app.dart';
import 'package:mobius_test/core/common/route.dart';

class MainApp extends StatelessWidget {
  const MainApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
    SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarBrightness: Brightness.light));
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus && currentFocus.focusedChild != null) {
          FocusManager.instance.primaryFocus?.unfocus();
        }
      },
      child: ScreenUtilInit(
        designSize: const Size(375, 816),
        minTextAdapt: true,
        splitScreenMode: true,
        builder: (context, child) {
          App.init();
          return GetMaterialApp(
            builder: (context, widget) {
              ScreenUtil.init(context);
              return MediaQuery(
                  data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0), child: widget!);
            },
            debugShowCheckedModeBanner: false,
            title: 'Todo',
            theme: App.theme?.lightTheme,
            darkTheme: App.theme?.darkTheme,
            themeMode: ThemeMode.light,
            getPages: AppPage.pages,
            initialRoute: AppPage.SPLASH,
            supportedLocales: const [
              Locale('en', 'US'),
              Locale('vi', 'VN'),
            ],
          );
        },
      ),
    );
  }
}
