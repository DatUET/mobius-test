import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AppStyle {
  /// Font size = 52.sp
  TextStyle? light52;
  TextStyle? regular52;
  TextStyle? medium52;
  TextStyle? semiBold52;
  TextStyle? bold52;
  TextStyle? extraBold52;

  /// Font size = 48.sp
  TextStyle? light48;
  TextStyle? regular48;
  TextStyle? medium48;
  TextStyle? semiBold48;
  TextStyle? bold48;
  TextStyle? extraBold48;

  /// Font size = 40.sp
  TextStyle? light40;
  TextStyle? regular40;
  TextStyle? medium40;
  TextStyle? semiBold40;
  TextStyle? bold40;
  TextStyle? extraBold40;

  /// Font size = 36.sp
  TextStyle? light36;
  TextStyle? regular36;
  TextStyle? medium36;
  TextStyle? semiBold36;
  TextStyle? bold36;
  TextStyle? extraBold36;

  /// Font size = 34.sp
  TextStyle? light34;
  TextStyle? regular34;
  TextStyle? medium34;
  TextStyle? semiBold34;
  TextStyle? bold34;
  TextStyle? extraBold34;

  /// Font size = 30.sp
  TextStyle? light30;
  TextStyle? regular30;
  TextStyle? medium30;
  TextStyle? semiBold30;
  TextStyle? bold30;
  TextStyle? extraBold30;

  /// Font size = 28.sp
  TextStyle? light28;
  TextStyle? regular28;
  TextStyle? medium28;
  TextStyle? semiBold28;
  TextStyle? bold28;
  TextStyle? extraBold28;

  /// Font size = 26.sp
  TextStyle? light26;
  TextStyle? regular26;
  TextStyle? medium26;
  TextStyle? semiBold26;
  TextStyle? bold26;
  TextStyle? extraBold26;

  /// Font size = 24.sp
  TextStyle? light24;
  TextStyle? regular24;
  TextStyle? medium24;
  TextStyle? semiBold24;
  TextStyle? bold24;
  TextStyle? extraBold24;

  /// Font size = 20.sp
  TextStyle? light20;
  TextStyle? regular20;
  TextStyle? medium20;
  TextStyle? semiBold20;
  TextStyle? bold20;
  TextStyle? extraBold20;

  /// Font size = 18.sp
  TextStyle? light18;
  TextStyle? regular18;
  TextStyle? medium18;
  TextStyle? semiBold18;
  TextStyle? bold18;
  TextStyle? extraBold18;

  /// Font size = 16.sp
  TextStyle? light16;
  TextStyle? regular16;
  TextStyle? medium16;
  TextStyle? semiBold16;
  TextStyle? bold16;
  TextStyle? extraBold16;

  /// Font size = 14.sp
  TextStyle? light14;
  TextStyle? regular14;
  TextStyle? medium14;
  TextStyle? semiBold14;
  TextStyle? bold14;
  TextStyle? extraBold14;

  /// Font size = 12.sp
  TextStyle? light12;
  TextStyle? regular12;
  TextStyle? medium12;
  TextStyle? semiBold12;
  TextStyle? bold12;
  TextStyle? extraBold12;

  /// Font size = 10.sp
  TextStyle? light10;
  TextStyle? regular10;
  TextStyle? medium10;
  TextStyle? semiBold10;
  TextStyle? bold10;
  TextStyle? extraBold10;

  /// Font size = 8.sp
  TextStyle? light8;
  TextStyle? regular8;
  TextStyle? medium8;
  TextStyle? semiBold8;
  TextStyle? bold8;
  TextStyle? extraBold8;

  AppStyle() {
    light52 = TextStyle(fontSize: 52.0.sp, fontWeight: FontWeight.w300);
    regular52 = TextStyle(fontSize: 52.0.sp, fontWeight: FontWeight.w400);
    medium52 = TextStyle(fontSize: 52.0.sp, fontWeight: FontWeight.w500);
    semiBold52 = TextStyle(fontSize: 52.0.sp, fontWeight: FontWeight.w600);
    bold52 = TextStyle(fontSize: 52.0.sp, fontWeight: FontWeight.w700);
    extraBold52 = TextStyle(fontSize: 52.0.sp, fontWeight: FontWeight.w800);

    light48 = TextStyle(fontSize: 48.0.sp, fontWeight: FontWeight.w300);
    regular48 = TextStyle(fontSize: 48.0.sp, fontWeight: FontWeight.w400);
    medium48 = TextStyle(fontSize: 48.0.sp, fontWeight: FontWeight.w500);
    semiBold48 = TextStyle(fontSize: 48.0.sp, fontWeight: FontWeight.w600);
    bold48 = TextStyle(fontSize: 48.0.sp, fontWeight: FontWeight.w700);
    extraBold48 = TextStyle(fontSize: 48.0.sp, fontWeight: FontWeight.w800);

    light40 = TextStyle(fontSize: 40.0.sp, fontWeight: FontWeight.w300);
    regular40 = TextStyle(fontSize: 40.0.sp, fontWeight: FontWeight.w400);
    medium40 = TextStyle(fontSize: 40.0.sp, fontWeight: FontWeight.w500);
    semiBold40 = TextStyle(fontSize: 40.0.sp, fontWeight: FontWeight.w600);
    bold40 = TextStyle(fontSize: 40.0.sp, fontWeight: FontWeight.w700);
    extraBold40 = TextStyle(fontSize: 40.0.sp, fontWeight: FontWeight.w800);

    light36 = TextStyle(fontSize: 36.0.sp, fontWeight: FontWeight.w300);
    regular36 = TextStyle(fontSize: 36.0.sp, fontWeight: FontWeight.w400);
    medium36 = TextStyle(fontSize: 36.0.sp, fontWeight: FontWeight.w500);
    semiBold36 = TextStyle(fontSize: 36.0.sp, fontWeight: FontWeight.w600);
    bold36 = TextStyle(fontSize: 36.0.sp, fontWeight: FontWeight.w700);
    extraBold36 = TextStyle(fontSize: 36.0.sp, fontWeight: FontWeight.w800);

    light34 = TextStyle(fontSize: 34.0.sp, fontWeight: FontWeight.w300);
    regular34 = TextStyle(fontSize: 34.0.sp, fontWeight: FontWeight.w400);
    medium34 = TextStyle(fontSize: 34.0.sp, fontWeight: FontWeight.w500);
    semiBold34 = TextStyle(fontSize: 34.0.sp, fontWeight: FontWeight.w600);
    bold34 = TextStyle(fontSize: 34.0.sp, fontWeight: FontWeight.w700);
    extraBold34 = TextStyle(fontSize: 34.0.sp, fontWeight: FontWeight.w800);

    light30 = TextStyle(fontSize: 30.0.sp, fontWeight: FontWeight.w300);
    regular30 = TextStyle(fontSize: 30.0.sp, fontWeight: FontWeight.w400);
    medium30 = TextStyle(fontSize: 30.0.sp, fontWeight: FontWeight.w500);
    semiBold30 = TextStyle(fontSize: 30.0.sp, fontWeight: FontWeight.w600);
    bold30 = TextStyle(fontSize: 30.0.sp, fontWeight: FontWeight.w700);
    extraBold30 = TextStyle(fontSize: 30.0.sp, fontWeight: FontWeight.w800);

    light28 = TextStyle(fontSize: 28.0.sp, fontWeight: FontWeight.w300);
    regular28 = TextStyle(fontSize: 28.0.sp, fontWeight: FontWeight.w400);
    medium28 = TextStyle(fontSize: 28.0.sp, fontWeight: FontWeight.w500);
    semiBold28 = TextStyle(fontSize: 28.0.sp, fontWeight: FontWeight.w600);
    bold28 = TextStyle(fontSize: 28.0.sp, fontWeight: FontWeight.w700);
    extraBold28 = TextStyle(fontSize: 28.0.sp, fontWeight: FontWeight.w800);

    light26 = TextStyle(fontSize: 26.0.sp, fontWeight: FontWeight.w300);
    regular26 = TextStyle(fontSize: 26.0.sp, fontWeight: FontWeight.w400);
    medium26 = TextStyle(fontSize: 26.0.sp, fontWeight: FontWeight.w500);
    semiBold26 = TextStyle(fontSize: 26.0.sp, fontWeight: FontWeight.w600);
    bold26 = TextStyle(fontSize: 26.0.sp, fontWeight: FontWeight.w700);
    extraBold26 = TextStyle(fontSize: 26.0.sp, fontWeight: FontWeight.w800);

    light24 = TextStyle(fontSize: 24.0.sp, fontWeight: FontWeight.w300);
    regular24 = TextStyle(fontSize: 24.0.sp, fontWeight: FontWeight.w400);
    medium24 = TextStyle(fontSize: 24.0.sp, fontWeight: FontWeight.w500);
    semiBold24 = TextStyle(fontSize: 24.0.sp, fontWeight: FontWeight.w600);
    bold24 = TextStyle(fontSize: 24.0.sp, fontWeight: FontWeight.w700);
    extraBold24 = TextStyle(fontSize: 24.0.sp, fontWeight: FontWeight.w800);

    light20 = TextStyle(fontSize: 20.0.sp, fontWeight: FontWeight.w300);
    regular20 = TextStyle(fontSize: 20.0.sp, fontWeight: FontWeight.w400);
    medium20 = TextStyle(fontSize: 20.0.sp, fontWeight: FontWeight.w500);
    semiBold20 = TextStyle(fontSize: 20.0.sp, fontWeight: FontWeight.w600);
    bold20 = TextStyle(fontSize: 20.0.sp, fontWeight: FontWeight.w700);
    extraBold20 = TextStyle(fontSize: 20.0.sp, fontWeight: FontWeight.w800);

    light18 = TextStyle(fontSize: 18.0.sp, fontWeight: FontWeight.w300);
    regular18 = TextStyle(fontSize: 18.0.sp, fontWeight: FontWeight.w400);
    medium18 = TextStyle(fontSize: 18.0.sp, fontWeight: FontWeight.w500);
    semiBold18 = TextStyle(fontSize: 18.0.sp, fontWeight: FontWeight.w600);
    bold18 = TextStyle(fontSize: 18.0.sp, fontWeight: FontWeight.w700);
    extraBold18 = TextStyle(fontSize: 18.0.sp, fontWeight: FontWeight.w800);

    light16 = TextStyle(fontSize: 16.0.sp, fontWeight: FontWeight.w300);
    regular16 = TextStyle(fontSize: 16.0.sp, fontWeight: FontWeight.w400);
    medium16 = TextStyle(fontSize: 16.0.sp, fontWeight: FontWeight.w500);
    semiBold16 = TextStyle(fontSize: 16.0.sp, fontWeight: FontWeight.w600);
    bold16 = TextStyle(fontSize: 16.0.sp, fontWeight: FontWeight.w700);
    extraBold16 = TextStyle(fontSize: 16.0.sp, fontWeight: FontWeight.w800);

    light14 = TextStyle(fontSize: 14.0.sp, fontWeight: FontWeight.w300);
    regular14 = TextStyle(fontSize: 14.0.sp, fontWeight: FontWeight.w400);
    medium14 = TextStyle(fontSize: 14.0.sp, fontWeight: FontWeight.w500);
    semiBold14 = TextStyle(fontSize: 14.0.sp, fontWeight: FontWeight.w600);
    bold14 = TextStyle(fontSize: 14.0.sp, fontWeight: FontWeight.w700);
    extraBold14 = TextStyle(fontSize: 14.0.sp, fontWeight: FontWeight.w800);

    light12 = TextStyle(fontSize: 12.0.sp, fontWeight: FontWeight.w300);
    regular12 = TextStyle(fontSize: 12.0.sp, fontWeight: FontWeight.w400);
    medium12 = TextStyle(fontSize: 12.0.sp, fontWeight: FontWeight.w500);
    semiBold12 = TextStyle(fontSize: 12.0.sp, fontWeight: FontWeight.w600);
    bold12 = TextStyle(fontSize: 12.0.sp, fontWeight: FontWeight.w700);
    extraBold12 = TextStyle(fontSize: 12.0.sp, fontWeight: FontWeight.w800);

    light10 = TextStyle(fontSize: 10.0.sp, fontWeight: FontWeight.w300);
    regular10 = TextStyle(fontSize: 10.0.sp, fontWeight: FontWeight.w400);
    medium10 = TextStyle(fontSize: 10.0.sp, fontWeight: FontWeight.w500);
    semiBold10 = TextStyle(fontSize: 10.0.sp, fontWeight: FontWeight.w600);
    bold10 = TextStyle(fontSize: 10.0.sp, fontWeight: FontWeight.w700);
    extraBold10 = TextStyle(fontSize: 10.0.sp, fontWeight: FontWeight.w800);

    light8 = TextStyle(fontSize: 8.0.sp, fontWeight: FontWeight.w300);
    regular8 = TextStyle(fontSize: 8.0.sp, fontWeight: FontWeight.w400);
    medium8 = TextStyle(fontSize: 8.0.sp, fontWeight: FontWeight.w500);
    semiBold8 = TextStyle(fontSize: 8.0.sp, fontWeight: FontWeight.w600);
    bold8 = TextStyle(fontSize: 8.0.sp, fontWeight: FontWeight.w700);
    extraBold8 = TextStyle(fontSize: 8.0.sp, fontWeight: FontWeight.w800);
  }
}
