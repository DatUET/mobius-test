import 'package:flutter/material.dart';
import 'package:mobius_test/core/app/main_app.dart';
import 'package:mobius_test/di/injection.dart';

Future<void> main() async {
  await Injector.setupData();
  await Injector.setupDomain();
  runApp(const MainApp());
}