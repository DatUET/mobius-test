import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobius_test/core/app/app.dart';
import 'package:mobius_test/presentation/home/controller/home_controller.dart';
import 'package:mobius_test/presentation/home/view/todo_item.dart';

class HomeScreen extends StatelessWidget {
  final HomeController _controller = Get.find<HomeController>();

  HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Todos'),
        backgroundColor: App.appColor?.primaryPlateColor,
        actions: [IconButton(onPressed: () => _controller.onTapAdd(), icon: const Icon(Icons.add))],
      ),
      body: Obx(
        () => ListView.builder(
          itemCount: _controller.todos.length,
          itemBuilder: (context, index) => TodoItem(
            todo: _controller.todos[index],
            onTapDone: () => _controller.onTapDoneTask(index),
            onTapDelete: () => _controller.onTapDeleteTask(index),
            onTap: () => _controller.onTapTaskItem(index),
          ),
        ),
      ),
    );
  }
}
