import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mobius_test/core/app/app.dart';
import 'package:mobius_test/domain/entities/todo.dart';

class TodoItem extends StatelessWidget {
  final Todo todo;
  final VoidCallback? onTapDone;
  final VoidCallback? onTapDelete;
  final VoidCallback? onTap;

  const TodoItem({
    Key? key,
    required this.todo,
    this.onTapDone,
    this.onTapDelete,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Card(
        child: Padding(
          padding: EdgeInsets.all(8.0.w),
          child: Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      todo.title ?? '',
                      style: App.appStyle?.bold24,
                    ),
                    SizedBox(
                      height: 8.h,
                    ),
                    Text(
                      todo.description ?? '',
                      style: App.appStyle?.medium16?.copyWith(
                        color: App.appColor?.gray1Color,
                      ),
                      maxLines: 2,
                    ),
                    SizedBox(
                      height: 8.h,
                    ),
                    Text(
                      'Trạng thái: ${(todo.isDone ?? false) ? 'Hoàn thành' : 'Đang thực hiện'}',
                    )
                  ],
                ),
              ),
              SizedBox(
                width: 4.w,
              ),
              if (!(todo.isDone ?? false))
                IconButton(onPressed: onTapDone, icon: const Icon(Icons.check)),
              SizedBox(
                width: 4.w,
              ),
              IconButton(
                onPressed: onTapDelete,
                icon: Icon(
                  Icons.close,
                  color: App.appColor?.errorColor,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
