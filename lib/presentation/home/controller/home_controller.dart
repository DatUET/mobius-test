import 'package:get/get.dart';
import 'package:mobius_test/core/common/route.dart';
import 'package:mobius_test/domain/entities/todo.dart';
import 'package:mobius_test/domain/use_case/todo_use_case.dart';

class HomeController extends GetxController {
  RxList<Todo> todos = RxList<Todo>();
  final TodoUseCase _todoUseCase = Get.find<TodoUseCase>();

  @override
  void onInit() {
    super.onInit();
    todos.value = _todoUseCase.getAll();
  }

  void refreshData() {
    todos.value = _todoUseCase.getAll();
    todos.refresh();
  }

  void onTapAdd() {
    Get.toNamed(AppPage.ADDITION_TASK);
  }

  void onTapDoneTask(int index) {
    int? id = todos[index].id;
    if (id != null) {
      _todoUseCase.doneTask(id);
      refreshData();
    }
  }

  void onTapDeleteTask(int index) {
    int? id = todos[index].id;
    if (id != null) {
      _todoUseCase.deleteTask(id);
      refreshData();
    }
  }

  void onTapTaskItem(int index) {
    int? id = todos[index].id;
    if (id != null) {
      Get.toNamed(AppPage.ADDITION_TASK, arguments: {'taskId': id});
    }
  }
}
