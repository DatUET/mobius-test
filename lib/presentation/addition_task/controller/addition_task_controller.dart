import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobius_test/core/common/enum.dart';
import 'package:mobius_test/domain/entities/todo.dart';
import 'package:mobius_test/domain/use_case/todo_use_case.dart';
import 'package:mobius_test/presentation/home/controller/home_controller.dart';

class AdditionTaskController extends GetxController {
  final TodoUseCase _todoUseCase = Get.find<TodoUseCase>();

  TextEditingController titleController = TextEditingController();
  TextEditingController desController = TextEditingController();
  Rx<ActionTodo> mode = Rx<ActionTodo>(ActionTodo.add);
  int? id;

  @override
  void onInit() {
    super.onInit();
    dynamic data = Get.arguments;
    if (data.toString().contains('taskId')) {
      mode.value = ActionTodo.update;
      id = data['taskId'];
      Todo? todo = _todoUseCase.getTaskWithId(id!);
      titleController.text = todo?.title ?? '';
      desController.text = todo?.description ?? '';
    }
  }

  void addTodo() {
    String title = titleController.text.trim();
    String desc = desController.text.trim();

    _todoUseCase.addTask(title: title, desc: desc);
    if (Get.isRegistered<HomeController>()) {
      Get.find<HomeController>().refreshData();
      Get.back();
    }
  }

  void updateTask() {
    String title = titleController.text.trim();
    String desc = desController.text.trim();
    if (id != null) {
      _todoUseCase.updateTask(id: id!, title: title, desc: desc);
      if (Get.isRegistered<HomeController>()) {
        Get.find<HomeController>().refreshData();
        Get.back();
      }
    }
  }
}
