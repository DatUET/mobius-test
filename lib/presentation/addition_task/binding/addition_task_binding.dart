import 'package:get/get.dart';
import 'package:mobius_test/presentation/addition_task/controller/addition_task_controller.dart';

class AdditionTaskBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => AdditionTaskController());
  }
}