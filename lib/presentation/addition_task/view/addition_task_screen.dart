import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:mobius_test/core/app/app.dart';
import 'package:mobius_test/core/common/enum.dart';
import 'package:mobius_test/presentation/addition_task/controller/addition_task_controller.dart';

class AdditionTaskScreen extends StatelessWidget {
  final AdditionTaskController _controller = Get.find<AdditionTaskController>();

  AdditionTaskScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:
            Obx(() => Text(_controller.mode.value == ActionTodo.add ? 'Add todo' : 'Update todo')),
        backgroundColor: App.appColor?.primaryPlateColor,
        actions: [
          IconButton(
              onPressed: () => _controller.mode.value == ActionTodo.add
                  ? _controller.addTodo()
                  : _controller.updateTask(),
              icon: const Icon(Icons.done))
        ],
      ),
      body: Padding(
        padding: EdgeInsets.all(16.w),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Title',
              style: App.appStyle?.semiBold16,
            ),
            SizedBox(
              height: 6.h,
            ),
            TextField(
              controller: _controller.titleController,
            ),
            SizedBox(height: 20.h),
            Text('Description',
              style: App.appStyle?.semiBold16,
            ),
            SizedBox(
              height: 6.h,
            ),
            TextField(
              controller: _controller.desController,
            ),
          ],
        ),
      ),
    );
  }
}
