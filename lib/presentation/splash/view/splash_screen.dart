import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobius_test/core/app/app.dart';
import 'package:mobius_test/presentation/splash/controller/splash_controller.dart';

class SplashScreen extends StatelessWidget {
  final SplashController _controller = Get.find<SplashController>();

  SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text('Todo App',
          style: App.appStyle?.bold36?.copyWith(
            color: App.appColor?.primaryColor,
          ),
        ),
      ),
    );
  }
}
