import 'package:get/get.dart';
import 'package:mobius_test/core/common/route.dart';

class SplashController extends GetxController {
  bool isLogin = false;

  @override
  void onInit() {
    super.onInit();
    Future.delayed(const Duration(milliseconds: 1400), () => Get.offAllNamed(AppPage.HOME));
  }
}
