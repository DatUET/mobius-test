import 'package:mobius_test/domain/entities/todo.dart';

abstract class TodoRepo {
  List<Todo> getAllTodos();

  Todo addTodo({required String title, required String description});

  Todo? getTodo({required int id});

  Todo updateTodo({required int id, String? title, String? description, bool? isDone});

  void deleteTodo({required int id});
}
