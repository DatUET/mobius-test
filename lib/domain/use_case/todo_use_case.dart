import 'package:mobius_test/domain/entities/todo.dart';
import 'package:mobius_test/domain/repositories/todo_repo.dart';

class TodoUseCase {
  final TodoRepo _todoRepo;

  TodoUseCase(this._todoRepo);

  List<Todo> getAll() {
    return _todoRepo.getAllTodos();
  }

  void doneTask(int id) {
    _todoRepo.updateTodo(id: id, isDone: true);
  }

  void deleteTask(int id) {
    _todoRepo.deleteTodo(id: id);
  }

  void addTask({
    required String title,
    required String desc,
  }) {
    _todoRepo.addTodo(title: title, description: desc);
  }

  Todo? getTaskWithId(int id) {
    return _todoRepo.getTodo(id: id);
  }

  void updateTask({
    required int id,
    required String title,
    required String desc,
  }) {
    _todoRepo.updateTodo(id: id, title: title, description: desc);
  }
}
