abstract class Todo {
  int? get id;

  String? get title;

  String? get description;

  bool? get isDone;
}
