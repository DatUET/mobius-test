import 'package:get/get.dart';
import 'package:mobius_test/data/models/todo_model.dart';

abstract class TodoLocalDataSource {
  List<TodoModel> getAllTodos();

  TodoModel addTodo({required String title, required String description});

  TodoModel? getTodo({required int id});

  TodoModel updateTodo({required int id, String? title, String? description, bool? isDone});

  void deleteTodo({required int id});
}

class TodoLocalDataSourceImpl implements TodoLocalDataSource {
  List<TodoModel> data = [];

  TodoLocalDataSourceImpl() {
    _setupData();
  }

  void _setupData() {
    for (var json in mockData) {
      TodoModel todo = TodoModel.fromJson(json);
      data.add(todo);
    }
  }

  @override
  List<TodoModel> getAllTodos() {
    return data;
  }

  @override
  TodoModel addTodo({required String title, required String description}) {
    TodoModel todo = TodoModel();
    todo.id = DateTime.now().microsecondsSinceEpoch;
    todo.title = title;
    todo.description = description;
    todo.isDone = false;
    data.add(todo);
    return todo;
  }


  @override
  TodoModel? getTodo({required int id}) {
    return data.firstWhereOrNull((todo) => todo.id == id);
  }

  @override
  TodoModel updateTodo({required int id, String? title, String? description, bool? isDone}) {
    TodoModel todo = data.firstWhere((element) => element.id == id);
    todo.title = title ?? todo.title;
    todo.description = description ?? todo.description;
    todo.isDone = isDone ?? todo.isDone;
    return todo;
  }

  @override
  void deleteTodo({required int id}) {
    data.removeWhere((todo) => todo.id == id);
  }
}

List<Map<String, dynamic>> mockData = [
  {
    "id": 1,
    "title": "Tập thể dục buổi sáng",
    "description": "Chống đẩy 20 cái vào lúc 7h sáng",
    "isDone": false
  },
  {
    "id": 2,
    "title": "Làm test Mobius",
    "description": "Làm bài test Mobius lúc 3h chiều",
    "isDone": false
  },
  {
    "id": 3,
    "title": "Đẩy app lên store",
    "description": "Đẩy app lên Google Play store và Apple store",
    "isDone": false
  },
  {
    "id": 4,
    "title": "Học thêm một công nghệ mới",
    "description": "Học NestJS để làm backend cho app",
    "isDone": false
  }
];
