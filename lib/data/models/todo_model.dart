import 'package:json_annotation/json_annotation.dart';
import 'package:mobius_test/domain/entities/todo.dart';

part 'todo_model.g.dart';

@JsonSerializable()
class TodoModel implements Todo {
  TodoModel({this.id, this.title, this.description, this.isDone});

  @override
  @JsonKey(name: 'id')
  int? id;

  @override
  @JsonKey(name: 'title')
  String? title;

  @override
  @JsonKey(name: 'description')
  String? description;

  @override
  @JsonKey(name: 'isDone')
  bool? isDone;

  factory TodoModel.fromJson(Map<String, dynamic> json) => _$TodoModelFromJson(json);

  Map<String, dynamic> toJson() => _$TodoModelToJson(this);
}
