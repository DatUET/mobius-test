import 'package:mobius_test/data/data_source/local/todo_local_datasource.dart';
import 'package:mobius_test/data/models/todo_model.dart';
import 'package:mobius_test/domain/entities/todo.dart';
import 'package:mobius_test/domain/repositories/todo_repo.dart';

class TodoRepoImpl implements TodoRepo {
  final TodoLocalDataSource _todoLocalDataSource;

  TodoRepoImpl(this._todoLocalDataSource);

  @override
  List<Todo> getAllTodos() {
    return _todoLocalDataSource.getAllTodos();
  }

  @override
  Todo addTodo({required String title, required String description}) {
    return _todoLocalDataSource.addTodo(title: title, description: description);
  }

  @override
  TodoModel? getTodo({required int id}) {
    return _todoLocalDataSource.getTodo(id: id);
  }

  @override
  TodoModel updateTodo({
    required int id,
    String? title,
    String? description,
    bool? isDone,
  }) {
    return _todoLocalDataSource.updateTodo(
        id: id, title: title, description: description, isDone: isDone);
  }

  @override
  void deleteTodo({required int id}) {
    return _todoLocalDataSource.deleteTodo(id: id);
  }
}
